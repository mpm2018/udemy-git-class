*** Settings ***
Library   String
Library   SeleniumLibrary

*** Variables ***
${browser}   chrome
${homepage}  automationpractice.com/index.php
${scheme}    http
${prodScheme}  https
${testUrl}   ${scheme}://${homepage}
${prodUrl}   ${prodScheme}://${homepage}

*** Keywords ***
Open Homepage
    Open Browser  ${testUrl}  ${browser}

*** Test Cases ***
C001-Hacer-Clic-en-Contenedores
   Open Homepage
   Set Global Variable  @{nombreDeContenedores}  //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a  //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
   :FOR     ${nombreDeContenedor}   IN   @{nombreDeContenedores}
   \  Run Keyword If  '${nombreDeContenedor}'=='//*[@id="homefeatured"]/li[7]/div/div[2]/h5/a'  Exit ForLoop
   \  Click Element  ${nombreDeContenedor}
   \  Wait until element is visible  //*[@id="bigpic"]
   \  Click Element  //*[@id="header_logo"]/a/img
   Close Browser
C002-Hacer-Caso-de-Prueba-Nuevo
    Open Homepage
    Close Browser

